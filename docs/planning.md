# Planning

You can find open tickets and work assigned in [our sprints](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=5849&useStoredSettings=true).

## Major Milestones

* 2021 Jan 05: [L7 Support and Security Groups Added to
  LBaaS](https://cern.service-now.com/service-portal?id=outage&n=OTG0061314)
* 2020 Apr 20: [Load Balancing as a Service (LBaaS) Generally Available](https://cern.service-now.com/service-portal?id=outage&n=OTG0055699)
* 2019 Oct 18: [Load Balancing as a Service (LBaaS) Pre-Production](https://cern.service-now.com/service-portal?id=outage&n=OTG0052861)

## Major Ongoing Work

* [Support Virtual Networks, Virtual Routing](https://its.cern.ch/jira/browse/OS-13964)
    * Status: validation phase, working deployment in sdn2 region
* [Integration with Physical Routers (SDN Gateway)](https://its.cern.ch/jira/browse/OS-9446)
    * Status: working prototype, not yet deployed in any of the regions
