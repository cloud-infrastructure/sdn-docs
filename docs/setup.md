# Setup

These are the components of our SDN setup:

* [OpenStack Neutron](https://wiki.openstack.org/wiki/Neutron): for our setup
  we rely only on the Neutron API with all calls being delegated to Tungsten
  Fabric
* [Tungsten Fabric (TF)](https://tungsten.io/): formerly known as OpenContrail, this
  is a Linux Foundation project initiated by Juniper. All network functions are
  handled by this layer, and all persistence is kept in its databases

## Architecture

A very brief overview of the architecture is shown below.

![Tungsten Fabric Architecture](imgs/tfarchitecture.png)

Some relevant points to take into account:

* The core of TF is in the configuration and analytics APIs and databases, and
  the controller which handles the interaction with the virtual routers
  (vRouter)
* Each hypervisor hosts a vRouter component handling the setup of the local
  network namespaces, tunnels and tap devices to be linked to the virtual
  machines. This can be done in IP fabric mode (less
  scalable) or relying on an external router acting as a SDN gateway
  (preferred)
* Tunneling can be done using MPLSoGRE, MPLSoUDP or VXLAN - right now we rely
  on VXLANs for all our setups

## Regions

We currently host two regions:

* *sdn1* is our production region, and where we already offer the [LBaaS](https://clouddocs.web.cern.ch/networking/load_balancing.html) service in production mode. Advanced
SDN functionality is not yet generally available
* *sdn2* is our testing region where we try new features and validate changes
  to be pushed later to *sdn1*

Both regions rely on a dedicated network environment where we can do tighter
integration between the overlay (virtualized) and underlay networking.

![Region Deployment Network](imgs/regionnetwork.png)
