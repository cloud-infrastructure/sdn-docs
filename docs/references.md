# References

## Repositories

### Upstream

* [Tungsten
  Fabric](https://github.com/tungstenfabric/)
* [Cloud Provider OpenStack](https://github.com/kubernetes/cloud-provider-openstack)

### Downstream

* [Contrail Neutron Plugin](https://gitlab.cern.ch/cloud-infrastructure/contrail-neutron-plugin/-/tree/cern)
* [Contrail Container Builder](https://gitlab.cern.ch/cloud-infrastructure/contrail-container-builder/-/tree/cern-R2005)
* [Contrail Helm Deployer](https://gitlab.cern.ch/cloud-infrastructure/contrail-helm-deployer)
* [Contrail Controller](https://gitlab.cern.ch/cloud-infrastructure/contrail-controller/-/tree/cern-r5.1)
* [Contrail API Client](https://gitlab.cern.ch/cloud-infrastructure/contrail-api-client/-/tree/cern-r5.1)
* [Tungsten Fabric Development Environment](https://gitlab.cern.ch/cloud-infrastructure/tf-dev-env/-/tree/cern)
* [Contrail Packages](https://gitlab.cern.ch/cloud-infrastructure/contrail-packages/-/tree/cern-r5.1)

## Resources

The control plane for both Tungsten and Neutron are running on Kubernetes
clusters in the `Cloud Infra Services` project.

### Region sdn1

* Tungsten: `cci-tf-controller-sdn1` cluster
* Tungsten Deployment: [Umbrella Chart](https://gitlab.cern.ch/cloud-infrastructure/tungsten/-/tree/sdn1-r5.1)
* Tungsten UI: https://webui-sdn.cern.ch:8143
* OpenStack/Neutron: `cci-openstack-plane-001` cluster
* OpenStack Deployment: [Umbrella Chart](https://gitlab.cern.ch/helm/releases/openstack/-/tree/sdn1)
* Compute Nodes: `ai-foreman -f sdn1 showhost`

### Region sdn2

* Tungsten: `cci-tf-controller-sdn2` cluster
* Tungsten UI: https://webui-sdn2.cern.ch/
* Tungsten Deployment: [Umbrella Chart](https://gitlab.cern.ch/cloud-infrastructure/tungsten/-/tree/sdn2-r5.1)
* OpenStack/Neutron: `cci-openstack-plane-stg` cluster
* OpenStack Deployment: [Umbrella Chart](https://gitlab.cern.ch/helm/releases/openstack/-/tree/sdn2)
* Compute Nodes: `ai-foreman -f sdn2 showhost`

### Service Accounts

* svctun: `tbag show svctun_password --hg cloud_networking/controller`

### Internal Documentation

* [LBaaS
  Troubleshooting](https://cci-internal-docs.web.cern.ch/lbaas/troubleshooting/)
