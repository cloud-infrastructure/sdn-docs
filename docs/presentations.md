# Presentations

## Internal

* Pre-GDB, 2021 Jun 07: [CERN Datacentre Virtualization Services](https://indico.cern.ch/event/1028690/)
* ASDF, 2021 May 27: [Update on Software Defined Networking deployment](https://indico.cern.ch/event/1034864/contributions/4345983/)
* CNIC, 2021 Apr 29: [SDN Features](https://indico.cern.ch/event/1022657/)
* Sprint, 2021 Feb 26: [Tungsten / SDN / LBaaS
  Walkthrough](https://indico.cern.ch/event/1011702/contributions/4245343/)
* ASDF, 2020 Dec 10: [LBaaS Security Groups and L7 Rules and Policies](https://indico.cern.ch/event/976468/contributions/4112397/)
* ASDF, 2020 May 14: [IP Based LBaaS](https://indico.cern.ch/event/913630/contributions/3842013/)
* ASDF, 2019 Sep 26: [IP Based Load Balancing
  Service](https://indico.cern.ch/event/848606/contributions/3565373/)
* ASDF, 2017 Dec 07: [Overview of OpenStack and
  SDN](https://indico.cern.ch/event/683999/)
* ASDF, 2017 Nov 02: [Data Centre Network Architecture
  WG](https://indico.cern.ch/event/671141/)

## External

* OpenStack Summit, 2018 May 21-24: [Evolution of OpenStack Networking at CERN:
  Nova Network, Neutron and SDN](https://www.openstack.org/summit/vancouver-2018/summit-schedule/events/20767/evolution-of-openstack-networking-at-cern-nova-network-neutron-and-sdn)
