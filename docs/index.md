# Software Defined Networking

The project aims to introduce software defined networks (SDN) at CERN to offer
additional isolation and flexibility in the network setup where needed.

This website includes details regarding the motivation, tools used, existing
deployments and additional references and presentation material.

## Project 

We currently run bi-weekly meetings for sprints and discussion.

* Every Second Friday 9:30am - [events here](https://indico.cern.ch/event/887298/)
* [Meeting Notes](https://codimd.web.cern.ch/oPCQsbbxTh2PmwRwUVJwPQ)
* [Sprints](https://its.cern.ch/jira/secure/RapidBoard.jspa?rapidView=5849&useStoredSettings=true)

## Communication

* [Mattermost](https://mattermost.web.cern.ch/it-dep/channels/neutronsdn)
* [Tungsten Fabric Slack (upstream)](https://app.slack.com/client/T0DQ21YM9)

