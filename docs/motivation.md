# Motivation

The general motivation for software defined networks is to offer dynamic and
programmatically (API based) network configuration, splitting the control plane
from the forwarding plane. This enables automated provisioning and policy based
management of network resources.

Below you can find more specific areas where this project can help improve the
networking setup at CERN. We plan to work gradually on each of these areas and
set priorities according to end user requirements.

## IP Mobility

CERN has a mostly flat network segmented in multiple broadcast domains
- primary IP services. When handling virtual machines (VMs) as in the OpenStack
case, the hypervisors get IP allocations in the primary service, and virtual
machines get IPs in secondary services associated with each primary.

This triggers a limitation in terms of IP mobility, as:

* the IP allocation for a VM can only be done after a hypervisor has been
  selected
* once a VM gets one or several IPs, VM migration can only be done within the
  IP service where those IPs have been allocated

A SDN based setup would allow crossing the boundaries between IP services and
opening up additional operational flexibility in IP assignment and VM
migration.

## Floating and Virtual IPs

The virtualization of the network layer allows IP assignments to existing
devices and interfaces managed purely by the network, with the targets
remaining unaware of their existence. Use cases where this can be useful include:

* Load Balancing as a Service (LBaaS) setups where clients contact a virtual IP
  pointing to a real or virtual resource that handles the traffic forwarding
  taking into account the client, backend load, request type, etc
* Moving IP allocations among multiple devices in a way transparent to the
  clients. This is useful when migrating services to new resources, retirement
  campaigns, etc

## Improved Isolation

The CERN network currently offers a limited level of network isolation - TN,
ITS, GPN - but there are cases where additional isolation could be useful.

One of the main goals of SDNs is to provide virtual/private networks. These can
be created on demand by projects or for a group of projects, can have well
defined gateways to other private or public networks, and can even overlap in
their IP ranges - useful for debug and testing setups.

## Security Groups

IP filter rules are traditionally managed by end users in their own instance setup,
and periodically checked by the security team. Security groups on the other hand
are named network access rules that limit the types of
traffic that have access to instances (inbound and outbound), but managed externally and associated with projects or instances.

Default security groups set default rules for all instances in a project or
network, and individually instances can have additional groups assigned. Groups
can be created and managed by project members, or can be global and managed by
the security team.

## Hardware Repurposing

CERN provides multiple network domains (TN, ITS, LHC) which require machines to
be plugged to the appropriate network device(s). If existing hardware is to be
repurposed to a different service requiring access to different domains this
involves manual steps and reconfiguration.

The virtualization of the network should also help improving the (re)assignment of
machines and devices to the existing domains.
